// eslint-disable-next-line import/prefer-default-export
export const validateBody = (body) => {
  const {
    items,
  } = body;

  if (items.length === 0) {
    return 'Need minimum 1 item.';
  }

  return '';
};
