// eslint-disable-next-line import/prefer-default-export
export const validateHeader = (header) => {
  const {
    from,
    billTo,
    invoiceNumber,
  } = header;

  if (!from) {
    return 'Who is the invoice from?';
  }

  if (!billTo.value) {
    return 'For who is the invoice for?';
  }

  if (!invoiceNumber) {
    return 'Invoice number is needed for invoice.';
  }

  return '';
};
