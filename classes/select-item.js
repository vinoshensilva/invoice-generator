class SelectItem {
  constructor(value, text) {
    this.value = value;
    this.text = text;
  }
}

export default SelectItem;
