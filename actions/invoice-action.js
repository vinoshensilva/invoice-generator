export const SET_CURRENCY = 'SET_CURRENCY';
export const SET_INVOICE_DATA = 'SET_INVOICE_DATA';

export const setCurrency = (currency, currencyKey) => ({
  type: SET_CURRENCY,
  payload: {
    currency,
    currencyKey,
  },
});

export const setInvoiceData = (invoice) => ({
  type: SET_INVOICE_DATA,
  payload: {
    invoice,
  },
});
