export const SET_VALUE = 'SET_VALUE';
export const SET_LABEL = 'SET_LABEL';
export const SET_HEADER = 'SET_HEADER';

export const setValue = (field, value) => ({
  type: SET_VALUE,
  payload: {
    field,
    value,
  },
});

export const setLabel = (field, label) => ({
  type: SET_LABEL,
  payload: {
    field,
    label,
  },
});

export const setHeader = (header) => ({
  type: SET_HEADER,
  payload: {
    header,
  },
});
