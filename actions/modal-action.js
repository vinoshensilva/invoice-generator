export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const openModal = (text, title, confirmFunc) => ({
  type: OPEN_MODAL,
  payload: {
    title,
    text,
    confirmFunc,
  },
});

export const closeModal = () => ({
  type: CLOSE_MODAL,
  payload: {},
});
