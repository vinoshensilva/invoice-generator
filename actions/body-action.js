export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';
export const CHANGE_LABEL = 'CHANGE_LABEL';
export const SET_BODY = 'SET_BODY';

export const changeLabel = (field, value) => ({
  type: CHANGE_LABEL,
  payload: {
    field,
    value,
  },
});

export const addItem = () => ({
  type: ADD_ITEM,
  payload: {},
});

export const removeItem = (index) => ({
  type: REMOVE_ITEM,
  payload: {
    index,
  },
});

export const updateItem = (index, field, value) => ({
  type: UPDATE_ITEM,
  payload: {
    index,
    field,
    value,
  },
});

export const setBody = (body) => ({
  type: SET_BODY,
  payload: {
    body,
  },
});
