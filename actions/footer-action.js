export const SET_FOOTER_VALUE = 'SET_FOOTER_VALUE';
export const SET_FOOTER_LABEL = 'SET_FOOTER_LABEL';
export const ADD_FIELD = 'ADD_FIELD';
export const REMOVE_FIELD = 'REMOVE_FIELD';
export const SET_FOOTER_FIELD_TYPE = 'SET_FOOTER_FIELD_TYPE';
export const SET_FOOTER = 'SET_FOOTER';

export const setValue = (field, value) => ({
  type: SET_FOOTER_VALUE,
  payload: {
    field,
    value,
  },
});

export const setFooter = (footer) => ({
  type: SET_FOOTER,
  payload: {
    footer,
  },
});

export const setLabel = (field, label) => ({
  type: SET_FOOTER_LABEL,
  payload: {
    field,
    label,
  },
});

export const addField = (field) => ({
  type: ADD_FIELD,
  payload: {
    field,
  },
});

export const removeField = (field) => ({
  type: REMOVE_FIELD,
  payload: {
    field,
  },
});

export const setFieldType = (field, type) => ({
  type: SET_FOOTER_FIELD_TYPE,
  payload: {
    field,
    type,
  },
});
