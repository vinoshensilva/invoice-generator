module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    borderWidth: {
      0: '0',
      0.5: '0.5px',
      1: '1px',
      2: '2px',
      3: '3px',
      4: '4px',
      6: '6px',
      8: '8px',
    },
    extend: {
      colors: {
        primary: '#3AAFA9',
        secondary: '#2B7A78',
        dark: '#17252A',
        light: '#FEFFFF',
        neutral: '#DEF2F1',
      },
      fontFamily: {
        outfit: ['Outfit', 'sans-serif'],
      },
    },
  },
  daisyui: {
    themes: false,
  },
};
