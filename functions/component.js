// eslint-disable-next-line import/prefer-default-export
export const getStyle = (classes, className) => {
  if (!classes || classes.length === 0) return '';

  if (!className) return classes[0];

  const style = classes.some((substring) => className.includes(substring)) ? className : `${classes[0]} ${className}`;

  return style;
};
