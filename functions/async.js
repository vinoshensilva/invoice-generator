// eslint-disable-next-line import/prefer-default-export
export const sleep = async (ms) => {
  // eslint-disable-next-line no-promise-executor-return
  await new Promise((resolve) => setTimeout(resolve, ms));
};
