// Import libraries
import { v4 as uuidv4 } from 'uuid';

// Import constants
import { percentage, value } from '@/constants/footer';

// Initial States
import { bodyReducerInitialState } from '@/reducers/body-reducer';
import { headerReducerInitialState } from '@/reducers/header-reducer';
import { footerReducerInitialState } from '@/reducers/footer-reducer';
import { invoiceInitialState } from '@/reducers/invoice-reducer';

export const calculateSubTotal = (items = []) => {
  let total = 0;

  items.forEach((item) => {
    total += item.quantity * item.rate;
  });

  return total;
};

function calculateDiscount(subTotal, discount) {
  let newAmount = subTotal;

  if (discount) {
    if (discount.type === percentage) {
      newAmount = subTotal - (subTotal * discount.value) / 100;
    } else if (discount.type === value) {
      newAmount = subTotal - discount.value;
    }
  }

  return newAmount;
}

function calculateTax(subTotal, tax) {
  let newAmount = subTotal;

  if (tax) {
    if (tax.type === percentage) {
      newAmount = subTotal - (subTotal * tax.value) / 100;
    } else if (tax.type === value) {
      newAmount = subTotal - tax.value;
    }
  }

  return newAmount;
}

function calculateShipping(total, shipping) {
  let newAmount = total;

  if (shipping) {
    newAmount = total - shipping.value;
  }

  return newAmount;
}

export const calculateTotal = (subTotal, discount, tax, shipping) => {
  let total = 0;

  total = calculateDiscount(subTotal, discount);

  total = calculateTax(total, tax);

  total = calculateShipping(total, shipping);

  return parseFloat(total).toFixed(2);
};

export const calculateBalance = (total, paid) => total - paid;

export const loadInvoices = () => {
  const invoices = JSON.parse(localStorage.getItem('invoices'));

  return invoices || {};
};

export const loadInvoice = (id) => {
  const invoices = loadInvoices();

  return invoices[id];
};

export const saveInvoiceById = (id, header, body, footer, invoice) => {
  const invoices = loadInvoices();

  if (invoices[id]) {
    const data = {
      header,
      body,
      footer,
      id,
      invoice,
    };

    invoices[id] = data;

    localStorage.setItem('invoices', JSON.stringify(invoices));
  }
};

export const deleteInvoiceById = (id) => {
  const invoices = loadInvoices();

  delete invoices[id];

  localStorage.setItem('invoices', JSON.stringify(invoices));
};

export const createInvoiceData = () => ({
  id: `${Date.now()}x${uuidv4()}`,
  header: headerReducerInitialState,
  body: bodyReducerInitialState,
  footer: footerReducerInitialState,
  invoice: invoiceInitialState,
});

export const createNewInvoice = () => {
  const data = createInvoiceData();

  const invoices = loadInvoices();

  const { id } = data;

  invoices[id] = data;

  localStorage.setItem('invoices', JSON.stringify(invoices));

  return data;
};
