// Import constants
import {
  AMOUNT_LABEL,
  ITEM_LABEL,
  QUANTITY_LABEL,
  RATE_LABEL,

  ITEM_DESCRIPTION,
  ITEM_QUANTITY,
  ITEM_RATE,
} from '@/constants/body';

// Import actions
import {
  ADD_ITEM,
  REMOVE_ITEM,
  UPDATE_ITEM,
  CHANGE_LABEL,
  SET_BODY,
} from '@/actions/body-action';
import { RESET } from '@/actions/reset-action';

function createNewItem() {
  return (
    {
      description: '',
      quantity: 0,
      rate: 0.00,
    }
  );
}

function addItem(state) {
  const newItem = createNewItem();

  const { items } = state;

  const copyItems = [...items];

  copyItems.push(newItem);

  const copyState = { ...state };

  copyState.items = copyItems;

  return copyState;
}

function removeItem(state, action) {
  const { payload } = action;

  const { index } = payload;

  const { items } = state;

  const copyItems = [...items];

  copyItems.splice(index);

  const copyState = { ...state };

  copyState.items = copyItems;

  return copyState;
}

function updateItem(state, action) {
  const { payload } = action;

  const { index, field, value } = payload;

  const { items } = state;

  const copyItems = [...items];

  const item = copyItems[index];

  switch (field) {
    case ITEM_DESCRIPTION:
    case ITEM_QUANTITY:
    case ITEM_RATE:
      item[field] = value;
      break;
    default:
      break;
  }

  const copyState = { ...state };

  copyState.items = copyItems;

  return copyState;
}

function changeLabel(state, action) {
  const { payload } = action;

  const { field, value } = payload;

  const copyState = { ...state };

  switch (field) {
    case ITEM_LABEL:
    case QUANTITY_LABEL:
    case RATE_LABEL:
    case AMOUNT_LABEL:
      copyState[field] = value;
      break;
    default:
      break;
  }

  return copyState;
}

export const bodyReducerInitialState = {
  items: [],
  itemLabel: 'Item',
  quantityLabel: 'Quantity',
  rateLabel: 'Rate',
  amountLabel: 'Amount',
};

function setBody(state, action) {
  const { body } = action.payload;

  const newState = {
    ...bodyReducerInitialState,
    ...body,
  };

  return newState;
}

export default function bodyReducer(state = bodyReducerInitialState, action = { type: '', payload: {} }) {
  switch (action.type) {
    case CHANGE_LABEL:
      return changeLabel(state, action);
    case ADD_ITEM:
      return addItem(state, action);
    case REMOVE_ITEM:
      return removeItem(state, action);
    case UPDATE_ITEM:
      return updateItem(state, action);
    case SET_BODY:
      return setBody(state, action);
    case RESET:
      return { ...bodyReducerInitialState };
    default:
      return state;
  }
}
