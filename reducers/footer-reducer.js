// Import constants
import {
  discount,
  notes,
  paid,
  percentage,
  shipping,
  tax,
  terms,
  value,
  subTotal,
  total,
  balance,
} from '@/constants/footer';

// Import action constants
import {
  SET_FOOTER_LABEL,
  SET_FOOTER_VALUE,
  ADD_FIELD,
  REMOVE_FIELD,
  SET_FOOTER_FIELD_TYPE,
  SET_FOOTER,
} from '@/actions/footer-action';
import { RESET } from '@/actions/reset-action';

function removeField(state, action) {
  const copyState = { ...state };

  const { field } = action.payload;

  switch (field) {
    case discount:
    case shipping:
    case tax:
      copyState[field] = undefined;
      break;
    default:
      break;
  }

  return copyState;
}

function addField(state, action) {
  const copyState = { ...state };

  const { field } = action.payload;

  const data = {
    type: percentage,
    value: 0,
  };

  switch (field) {
    case discount:
      copyState.discount = {
        label: 'Discount',
        ...data,
      };
      break;
    case shipping:
      copyState.shipping = {
        label: 'Shipping',
        value: 0,
      };
      break;
    case tax:
      copyState.tax = {
        label: 'Tax',
        ...data,
      };
      break;
    default:
      break;
  }

  return copyState;
}

function setValue(state, action) {
  const copyState = { ...state };

  // eslint-disable-next-line no-shadow
  const {
    field,
    // eslint-disable-next-line no-shadow
    value,
  } = action.payload;

  switch (field) {
    case discount:
    case tax:
    case shipping:
    case notes:
    case terms:
    case paid:
      if (copyState[field]) {
        copyState[field].value = value;
      }
      break;
    default:
      break;
  }

  return copyState;
}

function setLabel(state, action) {
  const copyState = { ...state };

  // eslint-disable-next-line no-shadow
  const {
    field,
    label,
  } = action.payload;

  switch (field) {
    case discount:
    case tax:
    case shipping:
    case notes:
    case terms:
    case subTotal:
    case paid:
    case total:
    case balance:
      if (copyState[field]) {
        copyState[field].label = label;
      }
      break;
    default:
      break;
  }

  return copyState;
}

function setFieldType(state, action) {
  const copyState = { ...state };

  // eslint-disable-next-line no-shadow
  const {
    field,
    type,
  } = action.payload;

  switch (field) {
    case discount:
    case tax:
      if ([value, percentage].includes(type)) {
        copyState[field].type = type;
      }
      break;
    default:
      break;
  }

  return copyState;
}

export const footerReducerInitialState = {
  subTotal: {
    label: 'Subtotal',
  },
  discount: undefined,
  tax: undefined,
  shipping: undefined,
  total: {
    label: 'Total',
  },
  paid: {
    label: 'Amount Paid',
    value: '',
  },
  balance: {
    label: 'Balance Due',
  },
  notes: {
    label: 'Notes',
    value: '',
  },
  terms: {
    label: 'Terms',
    value: '',
  },
};

function setFooter(state, action) {
  const { footer } = action.payload;

  const newState = {
    ...footerReducerInitialState,
    ...footer,
  };

  return newState;
}

export default function footerReducer(state = footerReducerInitialState, action = { type: '', payload: {} }) {
  switch (action.type) {
    case ADD_FIELD:
      return addField(state, action);
    case REMOVE_FIELD:
      return removeField(state, action);
    case SET_FOOTER_VALUE:
      return setValue(state, action);
    case SET_FOOTER_LABEL:
      return setLabel(state, action);
    case SET_FOOTER_FIELD_TYPE:
      return setFieldType(state, action);
    case SET_FOOTER:
      return setFooter(state, action);
    case RESET:
      return { ...footerReducerInitialState };
    default:
      return state;
  }
}
