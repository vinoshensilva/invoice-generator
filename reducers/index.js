// Import libraries
import {
  combineReducers,
} from 'redux';

// Import reducers
import headerReducer from '@/reducers/header-reducer';
import bodyReducer from '@/reducers/body-reducer';
import footerReducer from '@/reducers/footer-reducer';
import invoiceReducer from '@/reducers/invoice-reducer';

import modalReducer from '@/reducers/modal-reducer';

// Combined Reducers
const reducers = {
  headerReducer,
  bodyReducer,
  footerReducer,
  invoiceReducer,
  modalReducer,
};

export default combineReducers(reducers);
