// Import actions
import {
  SET_VALUE,
  SET_LABEL,
  SET_HEADER,
} from '@/actions/header-action';

import { RESET } from '@/actions/reset-action';

// Import constants
import {
  FROM,
  BILL_TO,
  INVOICE_NUMBER,
  INVOICE_NAME,
  DATE,
  DUE_DATE,
  LOGO,
  PO_NUMBER,
  SHIP_TO,
  TERMS,
} from '@/constants/header';

function setLabel(state, action) {
  const {
    field,
    label,
  } = action.payload;

  const header = state;

  switch (field) {
    case DATE:
    case DUE_DATE:
    case BILL_TO:
    case SHIP_TO:
    case PO_NUMBER:
    case TERMS:
      header[field].label = label;
      break;
    default:
      break;
  }

  return { ...header };
}

function setValue(state, action) {
  const {
    field,
    value,
  } = action.payload;

  const header = state;

  switch (field) {
    case INVOICE_NAME:
    case LOGO:
    case FROM:
    case INVOICE_NUMBER:
      header[field] = value;
      break;
    case SHIP_TO:
    case DATE:
    case DUE_DATE:
    case BILL_TO:
    case PO_NUMBER:
    case TERMS:
      header[field].value = value;
      break;
    default:
      break;
  }

  return { ...header };
}

export const headerReducerInitialState = {
  invoiceName: 'INVOICE',
  invoiceNumber: '',
  logo: '',
  from: '',

  billTo: {
    label: 'Bill To',
    value: '',
  },
  shipTo: {
    label: 'Ship To',
    value: '',
  },

  date: {
    label: 'Date',
    value: new Date(),
  },
  terms: {
    label: 'Payment Terms',
    value: '',
  },
  dueDate: {
    label: 'Due Date',
    value: new Date(),
  },
  poNumber: {
    label: 'PO Number',
    value: '',
  },
};

function setHeader(state, action) {
  const { header } = action.payload;

  const newState = {
    ...headerReducerInitialState,
    ...header,
  };

  if (!(newState.dueDate.value instanceof Date)) {
    newState.dueDate.value = new Date(newState.dueDate.value);
  }

  if (!(newState.date.value instanceof Date)) {
    newState.date.value = new Date(newState.date.value);
  }

  return newState;
}

export default function headerReducer(state = headerReducerInitialState, action = { type: '', payload: {} }) {
  switch (action.type) {
    case SET_VALUE:
      return setValue(state, action);
    case SET_LABEL:
      return setLabel(state, action);
    case SET_HEADER:
      return setHeader(state, action);
    case RESET:
      return { ...headerReducerInitialState };
    default:
      return state;
  }
}
