import {
  SET_CURRENCY,
  SET_INVOICE_DATA,
} from '@/actions/invoice-action';
import { RESET } from '@/actions/reset-action';

export const invoiceInitialState = {
  currency: '$',
  currencyKey: 'USD ($)',
};

function setCurrency(state, action) {
  const copyState = { ...state };

  const { currency, currencyKey } = action.payload;

  copyState.currency = currency;
  copyState.currencyKey = currencyKey;

  return copyState;
}

function setInvoiceData(state, action) {
  const {
    invoice,
  } = action.payload;

  const copyState = {
    ...invoiceInitialState,
    ...invoice,
  };

  return copyState;
}

export default function invoiceReducer(
  state = invoiceInitialState,
  action = { type: '', payload: {} },
) {
  switch (action.type) {
    case SET_CURRENCY:
      return setCurrency(state, action);
    case SET_INVOICE_DATA:
      return setInvoiceData(state, action);
    case RESET:
      return { ...invoiceInitialState };
    default:
      return state;
  }
}
