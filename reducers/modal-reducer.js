import {
  OPEN_MODAL,
  CLOSE_MODAL,
} from '@/actions/modal-action';

export const modalInitialState = {
  visible: false,
  confirmFunc: undefined,
  text: '',
  title: '',
};

function openModal(state, action) {
  const copyState = { ...state };

  copyState.visible = true;
  copyState.confirmFunc = action.payload.confirmFunc;
  copyState.text = action.payload.text;
  copyState.title = action.payload.title;

  return copyState;
}

function closeModal() {
  const copyState = { ...modalInitialState };

  return copyState;
}

export default function invoiceReducer(
  state = modalInitialState,
  action = { type: '', payload: {} },
) {
  switch (action.type) {
    case OPEN_MODAL:
      return openModal(state, action);
    case CLOSE_MODAL:
      return closeModal();
    default:
      return state;
  }
}
