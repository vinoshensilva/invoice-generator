import React, { useRef } from 'react';
import PropTypes from 'prop-types';

function TextArea({
  value,
  onChange,
  ...restProps
}) {
  const textAreaRef = useRef(undefined);

  function resizeTextArea() {
    if (textAreaRef.current) {
      textAreaRef.current.style.height = 'inherit';
      textAreaRef.current.style.height = `${textAreaRef.current.scrollHeight}px`;
    }
  }

  function onValueChanged(e) {
    resizeTextArea();
    onChange(e);
  }

  return (
    <textarea
      onChange={onValueChanged}
      value={value}
      ref={textAreaRef}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    />
  );
}

TextArea.defaultProps = {
  value: undefined,
};

TextArea.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

export default TextArea;
