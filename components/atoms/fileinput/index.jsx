/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useRef } from 'react';
import PropTypes from 'prop-types';

function FileInput({
  children,
  onChange,
  accept,
  name,
  ...restProps
}) {
  // Create a reference to the hidden file input input element
  const hiddenFileInput = useRef(null);

  // Programatically click the hidden file input element
  // when the Button component is clicked
  const handleClick = () => {
    hiddenFileInput.current.click();
  };

  const checkFile = (file) => !!file && accept.indexOf(file.type) !== -1;

  const onFileChange = (event) => {
    const { files } = event.target;

    if (files.length > 0) {
      const validType = [].every((file) => checkFile(file, file.type));

      if (validType) {
        onChange(files);
      }
    }
  };

  return (
    <label htmlFor={name} onClick={handleClick}>
      {children}
      <input
        type="file"
        accept={accept}
        onChange={onFileChange}
        name={name}
        ref={hiddenFileInput}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...restProps}
        className="hidden"
      />
    </label>
  );
}

FileInput.defaultProps = {
  children: undefined,
};

FileInput.propTypes = {
  accept: PropTypes.string.isRequired,
  children: PropTypes.node,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default FileInput;
