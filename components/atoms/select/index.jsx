// Import libraries
import React, { useState } from 'react';
import PropTypes from 'prop-types';

// Import components
import SelectItem from '@/classes/select-item';
import Button from '@/components/atoms/button';

function Select({
  value,
  onChange,
  options,
}) {
  const [show, setShow] = useState(false);

  function onFocus() {
    setShow(true);
  }

  function onClose() {
    setShow(false);
  }

  function renderOptions() {
    return options.map((option) => (
      <div
        className="cursor-pointer group"
        key={option.text}
      >
        <Button
          type="button"
          className={`p-2 w-full text-left border-transparent border-l-4 ${value === option.value ? ' border-primary ' : ' '} group-hover:border-primary group-hover:bg-gray-100`}
          onClick={() => {
            onClose();
            onChange(option);
          }}
        >
          {option.text}
        </Button>
      </div>
    ));
  }

  return (
    <div className="relative">
      <div className="h-10 bg-white flex border-1 hover:border-primary border-gray-200 rounded items-center">
        <input
          readOnly
          value={value}
          name="select"
          id="select"
          className="px-4 appearance-none outline-none cursor-pointer text-gray-800 w-full"
          onFocus={onFocus}
        />
      </div>
      <div className={`absolute max-h-60 rounded shadow bg-white overflow-y-auto ${show ? ' ' : ' hidden '} peer-checked:flex flex-col w-full mt-1 border border-gray-200`}>
        {renderOptions()}
      </div>
    </div>
  );
}

Select.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.instanceOf(SelectItem)).isRequired,
};

export default Select;
