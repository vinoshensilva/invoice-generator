import React from 'react';
import PropTypes from 'prop-types';

function TextInput({ onChange, ...restProps }) {
  return (
    <input
      onChange={onChange}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    />
  );
}

TextInput.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default TextInput;
