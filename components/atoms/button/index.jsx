import React from 'react';
import PropTypes from 'prop-types';

function Button({
  onClick, type, children, ...restProps
}) {
  return (
    <button
      // eslint-disable-next-line react/button-has-type
      type={type || 'button'}
      onClick={onClick}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    >
      {children}
    </button>
  );
}

Button.defaultProps = {
  children: undefined,
  onClick: undefined,
  type: 'button',
};

Button.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  type: PropTypes.string,
};

export default Button;
