// Import libraries
import React from 'react';
import PropTypes from 'prop-types';

import {
  Text,
  View,
  StyleSheet,
} from '@react-pdf/renderer';

// Import css in js
import {
  inputTertiaryStyle,
  textGray500Style,
  textWhiteStyle,
} from '@/css/index';

const styles = StyleSheet.create({
  itemHeaderRowStyle: {
    backgroundColor: 'rgb(17,24,39)',
    borderRadius: '6px',
    display: 'flex',
    flexDirection: 'row',
  },
  itemBodyRowStyle: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '2px',
    marginBottom: '2px',
  },
});

function Body({ body, currency }) {
  const {
    items,
    itemLabel,
    quantityLabel,
    rateLabel,
    amountLabel,
  } = body;

  return (
    <View>
      {/* Table Header */}
      <View
        style={styles.itemHeaderRowStyle}
      >
        <View style={{
          ...textWhiteStyle,
          ...inputTertiaryStyle,
          width: `${(100 / 6) * 3}%`,
        }}
        >
          <Text>
            {itemLabel}
          </Text>
        </View>
        <View style={{
          ...textWhiteStyle,
          ...inputTertiaryStyle,
          width: `${100 / 6}%`,
        }}
        >
          <Text>
            {quantityLabel}
          </Text>
        </View>
        <View style={{
          ...textWhiteStyle,
          ...inputTertiaryStyle,
          flex: 1,
          width: `${100 / 6}%`,
        }}
        >
          <Text>
            {rateLabel}
          </Text>
        </View>
        <View style={{
          ...textWhiteStyle,
          ...inputTertiaryStyle,
          flex: 1,
          width: `${100 / 6}%`,
        }}
        >
          <Text>
            {amountLabel}
          </Text>
        </View>
      </View>

      {/* Table Body */}
      <View>
        {
          items.map((item, i) => {
            const {
              description,
              quantity,
              rate,
            } = item;

            return (
              <View
                style={styles.itemBodyRowStyle}
                // eslint-disable-next-line react/no-array-index-key
                key={i}
              >
                <View style={{
                  textAlign: 'left',
                  width: `${(100 / 6) * 3}%`,
                  paddingBottom: '4px',
                  paddingTop: '4px',
                  paddingRight: '4px',
                  paddingLeft: '4px',
                  marginRight: '2px',
                  ...textGray500Style,
                }}
                >
                  <Text>
                    {description}
                  </Text>
                </View>
                <View style={{
                  textAlign: 'left',
                  width: `${100 / 6}%`,
                  paddingBottom: '4px',
                  paddingTop: '4px',
                  paddingRight: '4px',
                  paddingLeft: '4px',
                  marginRight: '2px',

                  ...textGray500Style,
                }}
                >
                  <Text>
                    {quantity}
                  </Text>
                </View>
                <View style={{
                  textAlign: 'left',
                  flex: 1,
                  width: `${100 / 6}%`,
                  paddingBottom: '4px',
                  paddingTop: '4px',
                  paddingRight: '4px',
                  paddingLeft: '4px',
                  marginRight: '2px',

                  ...textGray500Style,
                }}
                >
                  <Text>
                    {`${currency}${rate}`}
                  </Text>
                </View>
                <View style={{
                  flex: 1,
                  width: `${100 / 6}%`,
                  paddingBottom: '4px',
                  paddingTop: '4px',
                  paddingRight: '4px',
                  paddingLeft: '4px',
                  marginRight: '2px',
                  textAlign: 'right',
                  ...textGray500Style,
                }}
                >
                  <Text>
                    {`${currency}${(quantity * rate).toFixed(2)}`}
                  </Text>
                </View>
              </View>
            );
          })
        }
      </View>
    </View>
  );
}

Body.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  body: PropTypes.object.isRequired,
  currency: PropTypes.string.isRequired,
};

export default Body;
