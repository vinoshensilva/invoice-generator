// Import libraries
import React from 'react';
import PropTypes from 'prop-types';

import {
  Text,
  View,
} from '@react-pdf/renderer';

// Import functions
import {
  calculateBalance,
  calculateSubTotal,
  calculateTotal,
} from '@/functions/invoice';
import { inputSecondaryStyle } from '@/css/index';
import { percentage } from '@/constants/footer';

function Footer({
  footer,
  currency,
  items,
}) {
  const {
    discount,
    tax,
    shipping,
    subTotal,
    total,
    paid,
    balance,
    notes,
    terms,
  } = footer;

  const {
    label: totalLabel,
  } = total;

  const {
    label: subTotalLabel,
  } = subTotal;

  const {
    label: paidLabel,
    value: paidValue,
  } = paid;

  const {
    label: balanceLabel,
  } = balance;

  const {
    label: notesLabel,
    value: notesValue,
  } = notes;

  const {
    label: termsLabel,
    value: termsValue,
  } = terms;

  const subTotalCalc = calculateSubTotal(items);
  const totalCalc = calculateTotal(subTotalCalc, discount, tax, shipping);
  const balanceCalc = calculateBalance(totalCalc, paidValue);

  return (
    <View style={{
      display: 'flex',
      flexDirection: 'row',
    }}
    >
      <View
        style={{
          width: '50%',
          flex: 1,
        }}
      >
        {/* Notes */}
        <View
          style={{
            marginRight: '8px',
          }}
        >
          <Text
            style={{
              textAlign: 'left',
              marginBottom: '4px',
            }}
          >
            {notesLabel}
          </Text>

          {/* Notes Value */}
          <Text
            style={{
              paddingBottom: '18px',
            }}
          >
            {notesValue}
          </Text>
        </View>

        {/* Terms */}
        <View
          style={{ marginRight: '8px' }}
        >
          <Text
            style={{
              textAlign: 'left',
              marginBottom: '4px',
            }}
          >
            {termsLabel}
          </Text>

          {/* Terms Value */}
          <Text
            style={{
              paddingBottom: '18px',
            }}
          >
            {termsValue}
          </Text>
        </View>

      </View>
      <View
        style={{
          width: '50%',
          flex: 1,
        }}
      >
        {/* Date,Payment Terms, Due Date, and PO Number */}
        <View style={{
          marginLeft: '8px',
        }}
        >
          <View
            style={{
              marginBottom: '4px',
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1 }}>
                <Text style={inputSecondaryStyle}>
                  {subTotalLabel}
                </Text>
              </View>
              <View style={{
                flex: 1,
                paddingTop: '4px',
                textAlign: 'right',
              }}
              >
                <Text>
                  {`${currency}${subTotalCalc.toFixed(2)}`}
                </Text>
              </View>
            </View>
          </View>

          {
            discount
              ? (
                <View
              // eslint-disable-next-line react/no-array-index-key
                  style={{
                    marginBottom: '4px',
                  }}
                >
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <Text style={inputSecondaryStyle}>
                        {discount.label}
                      </Text>
                    </View>
                    <View style={{
                      flex: 1,
                      paddingTop: '4px',
                      textAlign: 'right',
                    }}
                    >
                      <Text>
                        {
                          discount.type === percentage
                            ? `${parseFloat(discount.value).toFixed(2)}%`
                            : `${currency}${parseFloat(discount.value).toFixed(2)}`
                        }
                      </Text>
                    </View>
                  </View>
                </View>
              ) : undefined
          }

          {
            [
              tax, shipping,
            ].map((field, i) => {
              if (field) {
                const {
                  label,
                  value,
                } = field;

                return (
                  <View
                    // eslint-disable-next-line react/no-array-index-key
                    key={i}
                    style={{
                      marginBottom: '4px',
                    }}
                  >
                    <View
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                      }}
                    >
                      <View style={{ flex: 1 }}>
                        <Text style={inputSecondaryStyle}>
                          {label}
                        </Text>
                      </View>
                      <View style={{
                        flex: 1,
                        paddingTop: '4px',
                        textAlign: 'right',
                      }}
                      >
                        <Text>
                          {`${currency}${parseFloat(value).toFixed(2)}`}
                        </Text>
                      </View>
                    </View>
                  </View>
                );
              }

              // eslint-disable-next-line react/jsx-no-useless-fragment
              return <></>;
            })
          }
          <View
            style={{
              marginBottom: '4px',
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1 }}>
                <Text style={inputSecondaryStyle}>
                  {totalLabel}
                </Text>
              </View>
              <View style={{
                flex: 1,
                paddingTop: '4px',
                textAlign: 'right',
              }}
              >
                <Text>
                  {`${currency}${parseFloat(totalCalc).toFixed(2)}`}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              marginBottom: '4px',
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1 }}>
                <Text style={inputSecondaryStyle}>
                  {paidLabel}
                </Text>
              </View>
              <View style={{
                flex: 1,
                paddingTop: '4px',
                textAlign: 'right',
              }}
              >
                <Text>
                  {`${currency}${parseFloat(paidValue).toFixed(2)}`}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              marginBottom: '4px',
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1 }}>
                <Text style={inputSecondaryStyle}>
                  {balanceLabel}
                </Text>
              </View>
              <View style={{
                flex: 1,
                paddingTop: '4px',
                textAlign: 'right',
              }}
              >
                <Text>
                  {`${currency}${balanceCalc.toFixed(2)}`}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

Footer.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  items: PropTypes.array.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  footer: PropTypes.object.isRequired,

  currency: PropTypes.string.isRequired,
};

export default Footer;
