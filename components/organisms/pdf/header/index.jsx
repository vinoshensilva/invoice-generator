// Import libraries
import React from 'react';
import PropTypes from 'prop-types';

import {
  Text,
  Image,
  View,
  StyleSheet,
} from '@react-pdf/renderer';

// Import css in js
import {
  inputSecondaryStyle,
  text4xlStyle,
  textBlackStyle,
} from '@/css/index';

const styles = StyleSheet.create({
  viewer: {
    width: '100%',
    height: '100vh',
  },
  page: {
    padding: '10px',
  },
  invoice: {
    backgroundColor: 'white',
    padding: '32px',
    grid: 1,
    gridAutoFlow: 'row',
    gap: '4px',
    fontSize: '12px',
    fontFamily: 'Outfit',
  },
  header: {
    width: '100%',
    grid: 1,
    gap: '12px',
    gridAutoFlow: 'row',
  },
  headerTop: {
    flexDirection: 'row',
  },
  logoContainer: {
    marginLeft: '0px',
    marginRight: '0px',
    width: '50%',
  },
  logo: {
    objectFit: 'contain',
    width: '192px',
    height: '128px',
  },
  invoiceDetailContainer: {
    width: '50%',
    display: 'grid',
    gap: '0px',
    alignContent: 'space-evenly',
  },
});

function Header({ header }) {
  const {
    invoiceName,
    invoiceNumber,
    logo,
    from,
    billTo,
    shipTo,
    date,
    terms,
    dueDate,
    poNumber,
  } = header;

  const {
    label: billToLabel,
    value: billToValue,
  } = billTo;

  const {
    label: shipToLabel,
    value: shipToValue,
  } = shipTo;

  const {
    label: dateLabel,
    value: dateValue,
  } = date;

  const {
    label: termsLabel,
    value: termsValue,
  } = terms;

  const {
    label: dueDateLabel,
    value: dueDateValue,
  } = dueDate;

  const {
    label: poNumberLabel,
    value: poNumberValue,
  } = poNumber;

  return (
    <View style={styles.header}>
      <View style={{
        ...styles.headerTop,
        marginBottom: '12px',
      }}
      >
        <View style={{
          ...styles.logoContainer,
        }}
        >
          {logo ? (
            <Image
              src={logo}
              style={styles.logo}
            />
          ) : undefined}
        </View>
        <View style={styles.invoiceDetailContainer}>
          <View>
            <Text
              style={{
                ...inputSecondaryStyle,
                ...textBlackStyle,
                ...text4xlStyle,
              }}
            >
              {invoiceName}
            </Text>
          </View>
          <View
            style={{
              width: '160px',
              marginLeft: 'auto',
              flexDirection: 'row',
            }}
          >
            <View>
              <Text
                style={{
                  textAlign: 'right',
                  width: '100%',
                }}
              >
                #
                {invoiceNumber}
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View style={{
        display: 'flex',
        flexDirection: 'row',
      }}
      >
        {/* From,Bill To, Ship To */}
        <View
          style={{
            width: '50%',
            flex: 1,
          }}
        >
          {/* From,Bill To, Ship To */}
          <View
            style={{
              grid: 1,
              gridAutoFlow: 'row',
              gap: '8px',
            }}
          >
            {/* From */}
            <View
              style={{
                width: '256px',
                marginBottom: '8px',
              }}
            >
              <Text
                style={{
                  // ...textAreaPrimary,
                  paddingBottom: '18px',
                }}
              >
                {from}
              </Text>
            </View>

            {/* Bill To, Ship To */}
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View
                style={{ flex: 1, marginRight: '8px' }}
              >
                {/* Bill To */}
                <Text
                  style={{
                    // ...inputSecondaryStyle,
                    textAlign: 'left',
                    marginBottom: '4px',
                  }}
                >
                  {billToLabel}
                </Text>

                {/* Bill To Value */}
                <Text
                  style={{
                    // ...textAreaPrimary,
                    paddingBottom: '18px',
                  }}
                >
                  {billToValue}
                </Text>
              </View>

              <View
                style={{ flex: 2 }}
              >
                {/* Ship To */}
                <Text
                  style={{
                    // ...inputSecondaryStyle,
                    textAlign: 'left',
                    marginBottom: '4px',
                  }}
                >
                  {shipToLabel}
                </Text>

                {/* Ship To Value */}
                <Text
                  style={{
                    // ...textAreaPrimary,
                    paddingBottom: '18px',
                  }}
                >
                  {shipToValue}
                </Text>
              </View>
            </View>
          </View>
        </View>

        {/* Date,Payment Terms, Due Date, and PO Number */}
        <View style={{
          flex: 1,
          marginLeft: '8px',
          width: '50%',
        }}
        >
          <View
            style={{
              marginBottom: '2px',
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1 }}>
                <Text
                  style={inputSecondaryStyle}
                >
                  {dateLabel}
                </Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text
                  style={{ paddingTop: '4px', textAlign: 'right' }}
                >
                  {dateValue.split('T')[0]}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              marginBottom: '2px',
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1 }}>
                <Text
                  style={inputSecondaryStyle}
                >
                  {termsLabel}
                </Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text
                  style={{ paddingTop: '4px', textAlign: 'right' }}
                >
                  {termsValue}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              marginBottom: '2px',
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1 }}>
                <Text
                  style={inputSecondaryStyle}
                >
                  {dueDateLabel}
                </Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text
                  style={{ paddingTop: '4px', textAlign: 'right' }}
                >
                  {dueDateValue.split('T')[0]}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              marginBottom: '2px',
            }}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
              }}
            >
              <View style={{ flex: 1 }}>
                <Text
                  style={inputSecondaryStyle}
                >
                  {poNumberLabel}
                </Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text
                  style={{ paddingTop: '4px', textAlign: 'right' }}
                >
                  {poNumberValue}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

Header.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  header: PropTypes.object.isRequired,
};

export default Header;
