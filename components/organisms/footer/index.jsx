// Import libraries
import React from 'react';

import {
  useDispatch,
  useSelector,
} from 'react-redux';

// Import components
import StyledTextInput from '@/components/molecules/styledinput/index';
import StyledTextArea from '@/components/molecules/styledtextarea/index';
import StyledButton from '@/components/molecules/styledbutton/index';
import Button from '@/components/atoms/button/index';

// Import actions
import {
  addField,
  removeField,
  setLabel,
  setValue,
  setFieldType,
} from '@/actions/footer-action';

// Import constants
import {
  discount as discountField,
  notes,
  paid,
  percentage,
  shipping as shippingField,
  subTotal,
  tax as taxField,
  terms,
  total,
} from '@/constants/footer';

import {
  calculateBalance,
  calculateSubTotal,
  calculateTotal,
} from '@/functions/invoice';

import CurrencyInput from '@/components/organisms/input/currency';

function Footer() {
  const dispatch = useDispatch();

  // Subtotal Field
  const subTotalLabel = useSelector((state) => state.footerReducer.subTotal.label);

  // Discount Field
  const footerReducerState = useSelector((state) => state.footerReducer);

  // Discount Field
  const discount = useSelector((state) => state.footerReducer.discount);

  // Tax Field
  const tax = useSelector((state) => state.footerReducer.tax);

  // Shipping Field
  const shipping = useSelector((state) => state.footerReducer.shipping);

  // Total Field
  const totalLabel = useSelector((state) => state.footerReducer.total.label);

  // Paid Field
  const paidLabel = useSelector((state) => state.footerReducer.paid.label);
  const paidValue = useSelector((state) => state.footerReducer.paid.value);

  // Balance Field
  const balanceLabel = useSelector((state) => state.footerReducer.balance.label);

  // Notes Field
  const notesLabel = useSelector((state) => state.footerReducer.notes.label);
  const notesValue = useSelector((state) => state.footerReducer.notes.value);

  // Terms Field
  const termsLabel = useSelector((state) => state.footerReducer.terms.label);
  const termsValue = useSelector((state) => state.footerReducer.terms.value);

  // Items Field
  const items = useSelector((state) => state.bodyReducer.items);

  // Currency Field
  const currency = useSelector((state) => state.invoiceReducer.currency);

  function onChangeValue(field, value) {
    dispatch(setValue(field, value));
  }

  function onChangeLabel(field, label) {
    dispatch(setLabel(field, label));
  }

  function onClickAddField(field) {
    dispatch(addField(field));
  }

  function onClickRemoveField(field) {
    dispatch(removeField(field));
  }

  function renderRemoveItemButton(field) {
    return (
      <div className="absolute left-full top-0 ml-2 text-lg">
        <Button
          onClick={() => { onClickRemoveField(field); }}
          className="font-outfit text-gray-400 font-bold hover:text-primary"
        >
          x
        </Button>
      </div>
    );
  }

  function renderField(field) {
    const data = footerReducerState[field];

    if (data) {
      const {
        type,
        value,
        label,
      } = data;

      return (
        <div key={field} className="flex">
          <div className="w-4/6">
            <StyledTextInput
              className="input--secondary"
              value={label}
              onChange={(e) => { onChangeLabel(field, e.target.value); }}
            />
          </div>
          <div className="w-2/6 relative font-outfit text-left flex items-center justify-end">

            {
              type ? (
                <CurrencyInput
                  currency={currency}
                  type={type}
                  value={value}
                  onChange={(e) => { onChangeValue(field, e.target.value); }}
                  onChangeType={
                    () => {
                      const newType = type === percentage ? 'value' : 'percentage';
                      dispatch(setFieldType(field, newType));
                    }
                  }
                />
              ) : (
                <StyledTextInput
                  value={value}
                  onChange={(e) => { onChangeValue(field, e.target.value); }}
                  type="number"
                />
              )
            }
            {renderRemoveItemButton(field)}
          </div>
        </div>
      );
    }

    return undefined;
  }

  function renderAddButtons() {
    const fields = [
      { data: discount, text: '+ Discount', field: discountField },
      { data: tax, text: '+ Tax', field: taxField },
      { data: shipping, text: '+ Shipping', field: shippingField },
    ];

    const arr = fields.map((x) => {
      if (!x.data) {
        return (
          <StyledButton
            key={x.field}
            className="button--secondary"
            onClick={() => { onClickAddField(x.field); }}
          >
            {x.text}
          </StyledButton>
        );
      }
      return undefined;
    });

    return arr;
  }

  const subTotalCalc = calculateSubTotal(items);
  const totalCalc = calculateTotal(subTotalCalc, discount, tax, shipping);

  const balanceCalc = calculateBalance(totalCalc, paidValue);

  return (
    <div className="grid content-start gap-1 grid-flow-row sm:grid-cols-2">

      {/* Notes and Term */}
      <div className="grid content-start gap-2 grid-flow-row">
        {/* Notes */}
        <div className="grid grid-flow-row gap-0.5">
          <StyledTextInput
            className="w-32 input--secondary text-left"
            value={notesLabel}
            onChange={(e) => { onChangeLabel(notes, e.target.value); }}
          />
          <StyledTextArea
            placeholder="Notes - any relevant information not already covered"
            value={notesValue}
            onChange={(e) => { onChangeValue(notes, e.target.value); }}
          />
        </div>

        {/* Terms */}
        <div className="grid grid-flow-row gap-0.5">
          <StyledTextInput
            className="w-32 input--secondary text-left"
            value={termsLabel}
            onChange={(e) => { onChangeLabel(terms, e.target.value); }}
          />
          <StyledTextArea
            placeholder="Terms and conditions - late fees, payment methods, delivery schedule"
            value={termsValue}
            onChange={(e) => { onChangeValue(terms, e.target.value); }}
          />
        </div>
      </div>

      {/* Subtotal,Discount,Tax,Shipping */}
      <div>
        <div className="grid grid-flow-row gap-1 content-start">

          {/* Subtotal */}
          <div className="flex">
            <div className="w-4/6">
              <StyledTextInput
                className="input--secondary"
                value={subTotalLabel}
                onChange={(e) => { onChangeLabel(subTotal, e.target.value); }}
              />
            </div>
            <div className="w-2/6 font-outfit text-right flex items-center justify-end">
              <span className="text-gray-300  mx-1">{currency}</span>
              {subTotalCalc}
            </div>
          </div>

          {
            [
              discountField,
              taxField,
              shippingField,
            ].map((field) => renderField(field))
          }

          <div className="flex justify-end">
            {renderAddButtons()}
          </div>

          {/* Total */}
          <div className="flex">
            <div className="w-3/4">
              <StyledTextInput
                className="input--secondary"
                value={totalLabel}
                onChange={(e) => { onChangeLabel(total, e.target.value); }}
              />
            </div>
            <div className="w-1/4 font-outfit text-right flex items-center justify-end">
              <span className="text-gray-300  mx-1">{currency}</span>
              {totalCalc}
            </div>
          </div>

          {/* Amount Paid */}
          <div className="flex">
            <div className="w-3/4">
              <StyledTextInput
                className="input--secondary"
                value={paidLabel}
                onChange={(e) => { onChangeLabel(paid, e.target.value); }}
              />
            </div>
            <div className="w-1/4 font-outfit text-left flex items-center justify-end">
              <StyledTextInput
                value={paidValue}
                onChange={(e) => { onChangeValue(paid, e.target.value); }}
              />
            </div>
          </div>

          {/* Balance Due */}
          <div className="flex">
            <div className="w-3/4">
              <StyledTextInput
                className="input--secondary"
                value={balanceLabel}
                onChange={(e) => { onChangeLabel(paid, e.target.value); }}
              />
            </div>
            <div className="w-1/4 font-outfit text-right flex items-center justify-end">
              <span className="text-gray-300  mx-1">{currency}</span>
              {balanceCalc}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
