// Import library
import React from 'react';
import {
  useDispatch,
  useSelector,
} from 'react-redux';

// Import components
import LogoInput from '@/components/molecules/logoinput/index';
import StyledTextInput from '@/components/molecules/styledinput/index';
import StyledTextArea from '@/components/molecules/styledtextarea/index';

// Import actions
import {
  setLabel,
  setValue,
} from '@/actions/header-action';

// Import constants
import {
  BILL_TO,
  DATE,
  DUE_DATE,
  FROM,
  INVOICE_NAME,
  INVOICE_NUMBER,
  LOGO,
  PO_NUMBER,
  SHIP_TO,
  TERMS,
} from '@/constants/header';
import { toBase64 } from '@/functions/image';
import DatePicker from '../../molecules/datepicker/index';

function Header() {
  const dispatch = useDispatch();

  // logo
  const logo = useSelector((state) => state.headerReducer.logo);

  // from
  const from = useSelector((state) => state.headerReducer.from);

  // invoiceName
  const invoiceName = useSelector((state) => state.headerReducer.invoiceName);

  // invoiceNumber
  const invoiceNumber = useSelector((state) => state.headerReducer.invoiceNumber);

  // billTo
  const billToLabel = useSelector((state) => state.headerReducer.billTo.label);
  const billToValue = useSelector((state) => state.headerReducer.billTo.value);

  // shipTo
  const shipToLabel = useSelector((state) => state.headerReducer.shipTo.label);
  const shipToValue = useSelector((state) => state.headerReducer.shipTo.value);

  // dateTo
  const dateToLabel = useSelector((state) => state.headerReducer.date.label);
  const dateToValue = useSelector((state) => state.headerReducer.date.value);

  // terms
  const termsLabel = useSelector((state) => state.headerReducer.terms.label);
  const termsValue = useSelector((state) => state.headerReducer.terms.value);

  // dueDate
  const dueDateLabel = useSelector((state) => state.headerReducer.dueDate.label);
  const dueDateValue = useSelector((state) => state.headerReducer.dueDate.value);

  // poNumber
  const poNumberLabel = useSelector((state) => state.headerReducer.poNumber.label);
  const poNumberValue = useSelector((state) => state.headerReducer.poNumber.value);

  const onChangeLogo = async (files) => {
    const { length } = files;

    if (length > 0) {
      const base64 = await toBase64(files[0]);

      dispatch(setValue(LOGO, base64));
    }
  };

  const onChangeLabel = (field, label) => {
    dispatch(setLabel(field, label));
  };

  const onChangeValue = (field, value) => {
    dispatch(setValue(field, value));
  };

  return (
    <div className="w-full grid grid-flow-row gap-3">
      {/* Logo, Invoice Name and Invoice Number */}
      <div className="grid grid-flow-row gap-2 sm:flex">
        <div className="mx-auto sm:mx-0 sm:w-1/2">
          {/* Logo */}
          <LogoInput
            onChange={onChangeLogo}
            name="logo"
            src={logo}
          />
        </div>
        <div className="gap-2 sm:gap-0 sm:w-1/2 grid content-evenly">
          <div>
            {/* Invoice Name */}
            <StyledTextInput
              className="input--secondary text-4xl text-black"
              value={invoiceName}
              onChange={(e) => {
                onChangeValue(INVOICE_NAME, e.target.value);
              }}
            />
          </div>

          <div className="w-full sm:w-40 sm:ml-auto flex">

            <div
              className="w-8 bg-gray-300 border-0.5 border-gray-300 rounded-md rounded-r-none flex items-center font-outfit font-light"
            >
              <div className="mx-auto">
                #
              </div>
            </div>

            {/* Invoice Number */}
            <div className="grow sm:grow-0">
              <StyledTextInput
                className="rounded-l-none"
                value={invoiceNumber}
                onChange={(e) => {
                  onChangeValue(INVOICE_NUMBER, e.target.value);
                }}
              />
            </div>
          </div>
        </div>
      </div>

      <div className="grid grid-flow-row sm:grid-flow-col gap-2">
        {/* From,Bill To, Ship To */}
        <div className="grid grid-flow-row gap-2">
          <div className="w-full sm:w-64">
            {/* from */}
            <StyledTextArea
              onChange={(e) => {
                onChangeValue(FROM, e.target.value);
              }}
              value={from}
              placeholder="Who is this from? required"
            />
          </div>

          <div className="sm:grid sm:grid-flow-col sm:gap-2">

            {/* Bill To */}
            <div className="grid grid-flow-row gap-2 content-start">
              <StyledTextInput
                value={billToLabel}
                onChange={(e) => {
                  onChangeLabel(BILL_TO, e.target.value);
                }}
                className="input--secondary text-left w-32"
              />
              <StyledTextArea
                value={billToValue}
                placeholder="Who is this for? (required)"
                onChange={(e) => {
                  onChangeValue(BILL_TO, e.target.value);
                }}
              />
            </div>

            {/* Ship To */}
            <div className="grid grid-flow-row gap-2 content-start">
              <StyledTextInput
                className="input--secondary text-left w-32"
                value={shipToLabel}
                onChange={(e) => {
                  onChangeLabel(SHIP_TO, e.target.value);
                }}
              />
              <StyledTextArea
                value={shipToValue}
                onChange={(e) => {
                  onChangeValue(SHIP_TO, e.target.value);
                }}
                placeholder="(optional)"
              />
            </div>
          </div>
        </div>

        {/* Date, Payment Terms, Due Date, and PO Number */}
        <div className="grid grid-flow-row gap-0.5 content-start">
          {/* Date */}
          <div className="grid grid-flow-col gap-1">
            <StyledTextInput
              className="input--secondary w-full"
              value={dateToLabel}
              onChange={(e) => {
                onChangeLabel(DATE, e.target.value);
              }}
            />
            <DatePicker
              currentDate={dateToValue}
              onChange={(date) => {
                onChangeValue(DATE, date);
              }}
            />
          </div>

          {/* Payment Terms */}
          <div className="grid grid-flow-col gap-1">
            <StyledTextInput
              className="input--secondary w-full"
              value={termsLabel}
              onChange={(e) => {
                onChangeLabel(TERMS, e.target.value);
              }}
            />
            <StyledTextInput
              value={termsValue}
              onChange={(e) => {
                onChangeValue(TERMS, e.target.value);
              }}
            />
          </div>

          {/* Due Date */}
          <div className="grid grid-flow-col gap-1">
            <StyledTextInput
              className="input--secondary w-full"
              value={dueDateLabel}
              onChange={(e) => {
                onChangeLabel(DUE_DATE, e.target.value);
              }}
            />
            <DatePicker
              currentDate={dueDateValue}
              onChange={(date) => {
                onChangeValue(DUE_DATE, date);
              }}
            />
          </div>

          {/* PO Number */}
          <div className="grid grid-flow-col gap-1">
            <StyledTextInput
              className="input--secondary w-full"
              value={poNumberLabel}
              onChange={(e) => {
                onChangeValue(PO_NUMBER, e.target.value);
              }}
            />
            <StyledTextInput
              value={poNumberValue}
              onChange={(e) => {
                onChangeValue(PO_NUMBER, e.target.value);
              }}
            />
          </div>

        </div>
      </div>

    </div>
  );
}

export default Header;
