// Import library
import React from 'react';

import {
  useDispatch,
  useSelector,
} from 'react-redux';

// Import actions
import {
  addItem,
  changeLabel,
  removeItem,
  updateItem,
} from '@/actions/body-action';

// Import components
import StyledButton from '@/components/molecules/styledbutton/index';

import StyledTextInput from '@/components/molecules/styledinput/index';
import Button from '@/components/atoms/button/index';
import CurrencyInput from '@/components/organisms/input/currency/index';

// Import constants
import {
  AMOUNT_LABEL,
  ITEM_DESCRIPTION,
  ITEM_LABEL,
  ITEM_QUANTITY,
  ITEM_RATE,
  QUANTITY_LABEL,
  RATE_LABEL,
} from '@/constants/body';

function Body() {
  const dispatch = useDispatch();

  const items = useSelector((state) => state.bodyReducer.items);
  const itemLabel = useSelector((state) => state.bodyReducer.itemLabel);
  const quantityLabel = useSelector((state) => state.bodyReducer.quantityLabel);
  const rateLabel = useSelector((state) => state.bodyReducer.rateLabel);
  const amountLabel = useSelector((state) => state.bodyReducer.amountLabel);

  const currency = useSelector((state) => state.invoiceReducer.currency);

  function onUpdateItem(index, field, value) {
    dispatch(updateItem(index, field, value));
  }

  function onRemoveItem(index) {
    dispatch(removeItem(index));
  }

  const onAddItem = () => {
    dispatch(addItem());
  };

  function onChangeLabel(field, value) {
    dispatch(changeLabel(field, value));
  }

  function renderItemsLine() {
    const arr = [];

    const { length } = items;

    for (let i = 0; i < length; i += 1) {
      const item = items[i];

      const {
        description,
        quantity,
        rate,
      } = item;

      arr.push(
        <tr key={i} className="item--row my-1 relative group ">

          <td className="absolute left-full top-0 ml-2 text-lg">
            <Button
              onClick={() => { onRemoveItem(i); }}
              className="font-outfit text-gray-400 font-bold hover:text-primary"
            >
              x
            </Button>
          </td>

          <td className="item--data--description">
            <StyledTextInput
              className="text-left"
              value={description}
              placeholder="Description of service or product"
              onChange={(e) => { onUpdateItem(i, ITEM_DESCRIPTION, e.target.value); }}
            />
          </td>
          <td>
            <StyledTextInput
              className="text-left"
              value={quantity}
              placeholder="Quantity"
              onChange={(e) => { onUpdateItem(i, ITEM_QUANTITY, e.target.value); }}
              type="number"
            />
          </td>
          <td>
            <CurrencyInput
              currency={currency}
              value={rate}
              onChange={(e) => { onUpdateItem(i, ITEM_RATE, e.target.value); }}
            />
          </td>
          <td className="text-right py-1 px-2 font-outfit font-light">
            <span className="text-gray-300 mx-1">{currency}</span>
            {quantity * rate}
          </td>
        </tr>,
      );
    }

    return arr;
  }

  function renderTable() {
    return (
      <table className="hidden sm:block">
        <thead>
          <tr className="item--row bg-gray-900 rounded-md ">
            {/* Item Label */}
            <th className="item--data--description">
              <StyledTextInput
                className="input--tertiary"
                value={itemLabel}
                onChange={(e) => { onChangeLabel(ITEM_LABEL, e.target.value); }}
              />
            </th>
            {/* Quantity Label */}
            <th>
              <StyledTextInput
                className="input--tertiary"
                value={quantityLabel}
                onChange={(e) => { onChangeLabel(QUANTITY_LABEL, e.target.value); }}
              />
            </th>
            {/* Rate Label */}
            <th>
              <StyledTextInput
                className="input--tertiary"
                value={rateLabel}
                onChange={(e) => { onChangeLabel(RATE_LABEL, e.target.value); }}
              />
            </th>
            {/* Amount Label */}
            <th>
              <StyledTextInput
                className="input--tertiary"
                value={amountLabel}
                onChange={(e) => { onChangeLabel(AMOUNT_LABEL, e.target.value); }}
              />
            </th>
          </tr>
        </thead>
        <tbody>
          {renderItemsLine()}
        </tbody>
      </table>
    );
  }

  function renderCards() {
    const arr = [];

    const { length } = items;

    for (let i = 0; i < length; i += 1) {
      const item = items[i];

      const {
        description,
        quantity,
        rate,
      } = item;

      arr.push(
        <div
          key={i}
          className="bg-primary w-full font-outfit shadow-md border-0.5 overflow-hidden border-primary rounded-md grid grid-flow-row p-4"
        >
          <div className="text-white flex justify-between">
            <div>
              Amount:
              {' '}
              {`${currency} ${quantity * rate}`}
            </div>
            <div>
              <Button
                onClick={() => { onRemoveItem(i); }}
                className="font-outfit text-white text-lg font-bold hover:text-primary"
              >
                x
              </Button>
            </div>
          </div>
          <div className="grid grid-flow-row gap-2">
            <div>
              <p className="text-white">Description</p>
              <StyledTextInput
                className="text-left"
                value={description}
                placeholder="Description of service or product"
                onChange={(e) => { onUpdateItem(i, ITEM_DESCRIPTION, e.target.value); }}
              />
            </div>
            <div>
              <p className="text-white">Quantity</p>
              <StyledTextInput
                className="text-left"
                value={quantity}
                placeholder="Quantity"
                onChange={(e) => { onUpdateItem(i, ITEM_QUANTITY, e.target.value); }}
                type="number"
              />
            </div>
            <div className="text-white">
              <p>Rate</p>
              <CurrencyInput
                currency={currency}
                value={rate}
                onChange={(e) => { onUpdateItem(i, ITEM_RATE, e.target.value); }}
                className="input--primary text-black w-full cursor-text text-left outline-none ring-0"
              />
            </div>
          </div>
        </div>,
      );
    }

    return (
      <div className="grid grid-flow-row gap-2 sm:hidden w-full">
        {arr}
      </div>
    );
  }

  return (
    <div className="grid grid-flow-row gap-1">
      {renderTable()}
      {renderCards()}
      <div>
        <StyledButton
          onClick={onAddItem}
        >
          + Line Item
        </StyledButton>
      </div>
    </div>
  );
}

export default Body;
