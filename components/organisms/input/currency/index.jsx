import React from 'react';
import PropTypes from 'prop-types';
import TextInput from '@/components/atoms/input/index';
import { percentage, value } from '@/constants/footer';
import StyledButton from '@/components/molecules/styledbutton/index';

import {
  BsPercent,
  BsArrowRepeat,
} from 'react-icons/bs';

function CurrencyInput({
  currency,
  type,
  onChangeType,
  ...restProps
}) {
  return (
    <div className="flex">
      <div className="w-9/12 flex">
        <div className="flex items-center mr-2">{currency}</div>
        <TextInput
          className="input--tertiary text-black w-full cursor-text text-left  outline-none ring-0"
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...restProps}
          type="number"
        />
      </div>
      <div className=" items-center flex text-sm">
        {type === percentage && <BsPercent />}
      </div>
      <div className="flex">
        {type && (
        <StyledButton
          onClick={onChangeType}
          className="button--secondary text-gray-400 hover:text-primary p-0"
        >
          <div className="h-full flex items-center">
            {type && <BsArrowRepeat />}
          </div>
        </StyledButton>
        )}
      </div>
    </div>
  );
}

CurrencyInput.defaultProps = {
  onChangeType: undefined,
  type: undefined,
};

CurrencyInput.propTypes = {
  currency: PropTypes.string.isRequired,
  type: PropTypes.oneOf([undefined, null, percentage, value]),
  onChangeType: PropTypes.func,
};

export default CurrencyInput;
