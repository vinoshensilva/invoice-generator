// Import libraries
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import Link from 'next/link';
import PropTypes from 'prop-types';

// Import components
import Header from '@/components/organisms/header/index';
import Body from '@/components/organisms/body/index';
import Footer from '@/components/organisms/footer/index';
import InvoiceCurrency from '@/components/molecules/invoicecurrency';
import StyledButton from '@/components/molecules/styledbutton/index';

// Import validate functions
import { validateBody } from '@/validate/validate-body';
import { validateHeader } from '@/validate/validate-header';

import {
  loadInvoice,
  saveInvoiceById,
} from '@/functions/invoice';

// Import actions
import { setFooter } from '@/actions/footer-action';
import { setHeader } from '@/actions/header-action';
import { setBody } from '@/actions/body-action';
import { setInvoiceData } from '@/actions/invoice-action';

function Invoice({ id }) {
  const dispatch = useDispatch();

  const bodyState = useSelector((state) => state.bodyReducer);
  const headerState = useSelector((state) => state.headerReducer);
  const footerState = useSelector((state) => state.footerReducer);

  const invoiceState = useSelector((state) => state.invoiceReducer);

  const router = useRouter();

  const canClick = () => {
    const error = validateBody(bodyState) || validateHeader(headerState);

    return !error;
  };

  const onClickSaveTemplate = () => {
    if (canClick()) {
      saveInvoiceById(
        id,
        headerState,
        bodyState,
        footerState,
        invoiceState,
      );
    }
  };

  useEffect(() => {
    if (router.isReady) {
      const invoice = loadInvoice(id);

      if (invoice) {
        const {
          footer,
          body,
          header,
          invoice: invoiceData,
        } = invoice;

        dispatch(setFooter(footer));
        dispatch(setHeader(header));
        dispatch(setBody(body));
        dispatch(setInvoiceData(invoiceData));
      } else {
        router.push('/');
      }
    }
  }, [router.isReady]);

  return (
    <div className="md:flex justify-center border-0.5 border-neutral shadow-md max-w-fit mx-auto">
      <div>
        <div className="bg-white border-r-1 border-gray-200 p-8 max-w-4xl mx-auto grid grid-flow-row gap-4">
          <Header />
          <Body />
          <Footer />
        </div>
      </div>
      <div className="p-5 grid grid-flow-row content-start gap-2 bg-white">

        <StyledButton
          onClick={onClickSaveTemplate}
          className={`button--secondary ${canClick() ? '  ' : ' text-gray-300 hover:text-primary cursor-not-allowed '}`}
        >
          Save Template
        </StyledButton>

        <InvoiceCurrency />

        <Link href="/invoices">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a>
            <StyledButton className="w-full">
              My Invoices
            </StyledButton>
          </a>
        </Link>

        <Link href={`/${id}/view`}>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a target="_blank">
            <StyledButton className="w-full">
              Generate
            </StyledButton>
          </a>
        </Link>

      </div>
    </div>
  );
}

Invoice.propTypes = {
  id: PropTypes.string.isRequired,
};

export default Invoice;
