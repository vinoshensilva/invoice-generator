// Import Library
import React, { useState } from 'react';
import PropTypes from 'prop-types';

import ReactDatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

function DatePicker({
  onChange,
  currentDate,
}) {
  const [startDate, setStartDate] = useState(currentDate);

  const onDateChanged = (date) => {
    setStartDate(date);
    onChange(date);
  };

  return (
    <ReactDatePicker
      className="input--primary"
      selected={startDate}
      onChange={onDateChanged}
      popperPlacement="auto"
    />
  );
}

DatePicker.propTypes = {
  currentDate: PropTypes.instanceOf(Date).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default DatePicker;
