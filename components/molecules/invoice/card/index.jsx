// Import libraries
import React from 'react';
import PropTypes from 'prop-types';

// Import components
import StyledButton from '@/components/molecules/styledbutton';

// Import functions
import { deleteInvoiceById } from '@/functions/invoice';
import { trigger } from '@/functions/event';
import { useDispatch } from 'react-redux';
import { openModal } from '@/actions/modal-action';

function InvoiceCard({ name, id }) {
  const dispatch = useDispatch();

  const onConfirmDelete = () => {
    deleteInvoiceById(id);
    trigger('setInvoices');
  };

  const onClickDelete = () => {
    dispatch(openModal(
      'Are you sure you want to delete this invoice?',
      'Delete Invoice',
      onConfirmDelete,
    ));
  };

  return (
    <div style={{ maxWidth: '156px' }} className="bg-primary mx-au font-outfit shadow-md border-0.5  border-primary rounded-md grid grid-flow-row">
      <div className=" text-white text-center p-1 text-lg max-w-full break-all">
        <p>{name}</p>
      </div>
      <div className="grid grid-flow-col  gap-2 p-5 justify-center">
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a href={`/${id}`}>
          <StyledButton className="button--secondary bg-white hover:bg-gray-100">
            Edit
          </StyledButton>
        </a>
        <StyledButton
          className="button--secondary bg-rose-500 hover:bg-rose-600 text-white"
          onClick={onClickDelete}
        >
          Delete
        </StyledButton>
      </div>
    </div>
  );
}

InvoiceCard.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default InvoiceCard;
