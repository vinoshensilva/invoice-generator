// Import libraries
import React from 'react';

import {
  useDispatch,
  useSelector,
} from 'react-redux';

// Import actions
import { closeModal } from '@/actions/modal-action';
import StyledButton from '../styledbutton/index';

function Modal() {
  const confirmFunc = useSelector((state) => state.modalReducer.confirmFunc);
  const visible = useSelector((state) => state.modalReducer.visible);
  const text = useSelector((state) => state.modalReducer.text);
  const title = useSelector((state) => state.modalReducer.title);

  const dispatch = useDispatch();

  const onClickConfirm = () => {
    if (typeof (confirmFunc) !== 'undefined') {
      confirmFunc();
      dispatch(closeModal());
    }
  };

  const onClickCancel = () => {
    dispatch(closeModal());
  };

  if (!visible) {
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <></>;
  }

  return (
    <div
      className=" bg-black bg-opacity-50 font-outfit fixed w-full h-100 inset-0 z-50 overflow-hidden flex justify-center items-center animated fadeIn faster"
    >
      <div
        className="border border-teal-500 modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto"
      >
        <div className="py-4 text-left px-6">
          <div className="flex justify-between items-center pb-3">
            <p className="text-2xl font-medium">{title}</p>
          </div>
          <div className="my-5">
            <p>
              {text}
            </p>
          </div>
          <div className="flex justify-end pt-2">
            <StyledButton
              onClick={onClickConfirm}
              className="mr-2"
            >
              Confirm
            </StyledButton>
            <StyledButton
              onClick={onClickCancel}
              className="bg-rose-500 hover:bg-rose-700"
            >
              Cancel
            </StyledButton>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
