// Import library
import React from 'react';
import PropTypes from 'prop-types';

// Import components
import FileInput from '@/components/atoms/fileinput/index';
import StyledButton from '@/components/molecules/styledbutton';

function LogoInput({
  src,
  ...restProps
}) {
  function renderContent() {
    if (!src) {
      return (
        <div className="w-full h-full flex align-middle">
          <div className="mx-auto my-auto font-normal text-gray-400">
            <span className="font-semibold text-2xl">+</span>
            {' '}
            Add Your Logo
          </div>
        </div>
      );
    }

    return (
      <img
        src={src}
        alt="logo"
        className="w-full h-full object-contain"
      />
    );
  }

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <FileInput {...restProps} accept="image/*">
      <StyledButton
        className="button--logo"
      >
        {renderContent()}
      </StyledButton>
    </FileInput>
  );
}

LogoInput.defaultProps = {
  src: '',
};

LogoInput.propTypes = {
  src: PropTypes.string,
};

export default LogoInput;
