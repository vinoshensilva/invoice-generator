// Import library
import React from 'react';

// Import components
import TextArea from '@/components/atoms/textarea';

// Import function
import { getStyle } from '@/functions/component';

const CLASSES = [
  'textarea--primary',
];

function StyledTextArea({
  value,
  onChange,
  className,
  ...restProps
}) {
  const style = getStyle(CLASSES, className);

  return (
    <TextArea
      onChange={onChange}
      className={style}
      value={value}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    />
  );
}

StyledTextArea.defaultProps = {
  ...TextArea.defaultProps,
};

StyledTextArea.propTypes = {
  ...TextArea.propTypes,
};

export default StyledTextArea;
