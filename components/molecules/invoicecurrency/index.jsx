import React from 'react';

import {
  useDispatch,
  useSelector,
} from 'react-redux';

// Import components
import Select from '@/components/atoms/select';

// Import actions
import { setCurrency } from '@/actions/invoice-action';

// Import data
import { currencyOptions } from '@/data/currency';

function InvoiceCurrency() {
  const currencyKey = useSelector((state) => state.invoiceReducer.currencyKey);

  const dispatch = useDispatch();

  const onChangeCurrency = (val) => {
    dispatch(setCurrency(val.value, val.text));
  };

  return (
    <Select
      options={currencyOptions}
      onChange={onChangeCurrency}
      value={currencyKey}
    />
  );
}

export default InvoiceCurrency;
