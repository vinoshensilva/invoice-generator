// import Library
import React from 'react';

// import Components
import Button from '@/components/atoms/button';

// Import function
import { getStyle } from '@/functions/component';

const CLASSES = [
  'button--primary',
  'button--secondary',
  'button--logo',
];

function StyledButton({
  value,
  onClick,
  className,
  ...restProps
}) {
  const style = getStyle(CLASSES, className);

  return (
    <Button
      onClick={onClick}
      className={style}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    />
  );
}

StyledButton.defaultProps = {
  ...Button.defaultProps,
};

StyledButton.propTypes = {
  ...Button.propTypes,
};

export default StyledButton;
