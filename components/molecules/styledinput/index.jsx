// Import Library
import React from 'react';

// Import components
import TextInput from '@/components/atoms/input/index';

// Import function

import { getStyle } from '@/functions/component';

const CLASSES = [
  'input--primary',
  'input--secondary',
  'input--tertiary',
];

function StyledTextInput({
  value,
  onChange,
  className,
  ...restProps
}) {
  const style = getStyle(CLASSES, className);

  return (
    <TextInput
      onChange={onChange}
      className={style}
      value={value}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    />
  );
}

StyledTextInput.defaultProps = {
  ...TextInput.defaultProps,
};

StyledTextInput.propTypes = {
  ...TextInput.propTypes,
};

export default StyledTextInput;
