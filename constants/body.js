export const ITEMS = 'items';

export const ITEM_LABEL = 'itemLabel';
export const QUANTITY_LABEL = 'quantityLabel';
export const RATE_LABEL = 'rateLabel';
export const AMOUNT_LABEL = 'amountLabel';

export const ITEM_DESCRIPTION = 'description';
export const ITEM_QUANTITY = 'quantity';
export const ITEM_RATE = 'rate';
