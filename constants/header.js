export const INVOICE_NAME = 'invoiceName';
export const INVOICE_NUMBER = 'invoiceNumber';
export const LOGO = 'logo';
export const FROM = 'from';

export const BILL_TO = 'billTo';
export const SHIP_TO = 'shipTo';

export const DATE = 'date';
export const TERMS = 'terms';
export const DUE_DATE = 'dueDate';
export const PO_NUMBER = 'poNumber';
