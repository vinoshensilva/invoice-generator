export const discount = 'discount';
export const tax = 'tax';
export const shipping = 'shipping';

export const notes = 'notes';
export const terms = 'terms';
export const subTotal = 'subTotal';
export const total = 'total';
export const balance = 'balance';

export const paid = 'paid';

export const percentage = 'percentage';
export const value = 'value';
