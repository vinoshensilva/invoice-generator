// Import libraries
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

import {
  Page,
  View,
  Document,
  PDFViewer,
  StyleSheet,
  Font,
} from '@react-pdf/renderer';

// Import functions
import { loadInvoices } from '@/functions/invoice';

// Import components
import Header from '@/components/organisms/pdf/header/index';
import Body from '@/components/organisms/pdf/body/index';
import Footer from '@/components/organisms/pdf/footer/index';

// Register font
Font.register({ family: 'Outfit', src: '/fonts/Outfit-Regular.ttf' });

const styles = StyleSheet.create({
  viewer: {
    width: '100%',
    height: '100vh',
  },
  page: {
    padding: '10px',
  },
  invoice: {
    backgroundColor: 'white',
    padding: '32px',
    grid: 1,
    gridAutoFlow: 'row',
    gap: '4px',
    fontSize: '12px',
    fontFamily: 'Outfit',
  },
});

function ViewPage() {
  const router = useRouter();

  const [invoice, setInvoice] = useState(undefined);

  const { id } = router.query;

  useEffect(() => {
    if (router.isReady) {
      // Check if id in route query is valid
      if (!id) {
        router.push('/');
      } else {
        const invoices = loadInvoices();

        const data = invoices[id];

        if (!data) {
          router.push('/');
        }

        setInvoice(data);
      }
    }
  }, [router.isReady]);

  if (!router.isReady || !invoice) {
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <></>;
  }

  return (
    <PDFViewer
      style={styles.viewer}
    >
      <Document>
        <Page
          size="A4"
          style={styles.page}
        >
          <View style={styles.invoice}>
            <View style={{ marginBottom: '8px' }}>
              <Header
                header={invoice.header}
              />
            </View>
            <View style={{ marginBottom: '8px' }}>
              <Body
                body={invoice.body}
                currency={invoice.invoice.currency}
              />
            </View>
            <Footer
              footer={invoice.footer}
              currency={invoice.invoice.currency}
              items={invoice.body.items}
            />
          </View>
        </Page>
      </Document>
    </PDFViewer>
  );
}

export default ViewPage;
