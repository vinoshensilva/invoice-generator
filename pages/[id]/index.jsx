// Import library
import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';

// Import components
import Invoice from '@/components/organisms/invoice';
import { loadInvoices } from '@/functions/invoice';
import { reset } from '@/actions/reset-action';

function Home() {
  const router = useRouter();

  const { id } = router.query;

  const dispatch = useDispatch();

  useEffect(() => {
    if (router.isReady) {
      // Check if id in route query is valid
      if (!id) {
        router.push('/');
      } else {
        const invoices = loadInvoices();

        if (!invoices[id]) {
          router.push('/');
        }
      }
    }

    return () => {
      dispatch(reset());
    };
  }, [router.isReady]);

  if (!router.isReady) {
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <></>;
  }

  return (
    <div className="h-screen w-full bg-gray-100">
      <div className="md:p-20">
        <Invoice
          id={id}
        />
      </div>
    </div>
  );
}

export default Home;
