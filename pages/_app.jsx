// Import libraries
import React from 'react';
import '../styles/globals.css';
import Head from 'next/head';

import { Provider } from 'react-redux';
import { useStore } from '@/store/index';

// Import components
import Modal from '@/components/molecules/modal/index';

// eslint-disable-next-line react/prop-types
function MyApp({ Component, pageProps }) {
  // eslint-disable-next-line react/prop-types
  const store = useStore(pageProps.initialReduxState);

  return (
    <>
      <Head>
        <title>Invoice Generator</title>
      </Head>
      <Provider store={store}>
        <Modal />
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <Component {...pageProps} />
      </Provider>
    </>
  );
}

export default MyApp;
