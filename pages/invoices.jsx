// Import libraries
import React, {
  useEffect,
  useState,
} from 'react';

import Link from 'next/link';

// Import functions
import { loadInvoices } from '@/functions/invoice';
import { on, off } from '@/functions/event';

// Import components
import InvoiceCard from '@/components/molecules/invoice/card/index';
import StyledButton from '@/components/molecules/styledbutton/index';

function Invoices() {
  const [invoices, setInvoices] = useState({});

  const getInvoices = () => {
    const savedInvoices = loadInvoices();
    setInvoices(savedInvoices);
  };

  useEffect(() => {
    getInvoices();

    on('setInvoices', getInvoices);

    return () => {
      off('setInvoices', getInvoices);
    };
  }, []);

  function renderInvoiceCards() {
    const keys = Object.keys(invoices);

    return keys.map((key) => {
      const invoice = invoices[key];

      const {
        header,
      } = invoice;

      const {
        invoiceName,
      } = header;

      return (
        <InvoiceCard
          id={key}
          key={key}
          name={invoiceName}
        />
      );
    });
  }

  return (
    <div className="h-screen w-full bg-gray-100">
      <div className="p-20">
        <div className="text-center">
          <h1 className="font-outfit text-3xl text-primary">Invoices</h1>
        </div>
        <div className="text-center mt-2">
          <Link href="/">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>
              <StyledButton>
                Create New Invoice
              </StyledButton>
            </a>
          </Link>
        </div>
        <div className="mt-4">
          {
              Object.keys(invoices).length === 0 && (
                <div className="text-center">
                  No invoices currently
                </div>
              )
          }
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 xl:grid-cols-8 gap-2">
            {
              Object.keys(invoices).length !== 0 && renderInvoiceCards()
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default Invoices;
