// Import libraries
import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { createNewInvoice } from '@/functions/invoice';

function Home() {
  const router = useRouter();

  useEffect(() => {
    if (router.isReady) {
      const newInvoice = createNewInvoice();

      const { id } = newInvoice;

      router.push(`/${id}`);
    }
  }, [router.isReady]);

  return (
    <>
    </>
  );
}

export default Home;
