import {
  StyleSheet,
} from '@react-pdf/renderer';

export const text4xlStyle = StyleSheet.create({
  fontSize: '36px',
  marginTop: '20px',
  marginBottom: '20px',
});

export const textBlackStyle = StyleSheet.create({
  color: 'rgb(0,0,0)',
});

export const textWhiteStyle = StyleSheet.create({
  color: 'rgb(255,255,255)',
});

export const textGray500Style = StyleSheet.create({
  color: 'rgb(107,114,128)',
});

export const inputStyle = StyleSheet.create({
  textAlign: 'right',
  fontWeight: '300',
  width: '100%',
  paddingTop: '4px',
  paddingBottom: '2px',
  paddingLeft: '6px',
  paddingRight: '6px',
  borderRadius: '6px',
  minHeight: '25px',
  borderColor: 'rgb(209,213,219)',
});

export const inputPrimaryStyle = StyleSheet.create({
  ...inputStyle,
  borderWidth: '0.5px',
});

export const inputSecondaryStyle = StyleSheet.create({
  ...inputStyle,
  ...textGray500Style,
  borderWidth: '0px',
  borderColor: 'rgba(229,231,235,0)',
});

export const inputTertiaryStyle = StyleSheet.create({
  ...inputStyle,
  borderWidth: '0px',
  ...textWhiteStyle,
  textAlign: 'left',
});

export const textAreaPrimary = StyleSheet.create({
  ...inputPrimaryStyle,
  textAlign: 'left',
  resize: 'none',
  overflowY: 'hidden',
});
